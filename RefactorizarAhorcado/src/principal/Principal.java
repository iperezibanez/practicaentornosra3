package principal;

import java.io.File;
import java.util.Scanner;

public class Principal {
	private final static byte NUM_PALABRAS = 20;
	private final static byte FALLOS = 7;
	private static String[] arrayPalabras = new String[NUM_PALABRAS];
	private final static String RUTA = "src\\palabras.txt";
	private static char[][] caracteresPalabra = new char[2][];
	private final static char COINCIDENCIA = '1';
	private final static String RAYA = " -";
	private final static String ESPACIO = " ";

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		String palabraSecreta = "";
		String caracteresElegidos = "";
		boolean acertado = true;
		boolean encontrado = false;
		int fallos = 0;

		llenarArrayPalabras(arrayPalabras, RUTA);

		palabraSecreta = obtencionPalabra(palabraSecreta);

		inicializarArrays(caracteresPalabra, palabraSecreta);

		System.out.println("Acierta la palabra");

		desarrolloJuego(caracteresPalabra, input, fallos, encontrado, caracteresElegidos, FALLOS, acertado,
				palabraSecreta);

		input.close();
	}

	public static void desarrolloJuego(char[][] caracteresPalabra, Scanner input, int fallos, boolean encontrado,
			String caracteresElegidos, byte FALLOS, boolean acertado, String palabraSecreta) {

		do {

			generarInterfaz(caracteresPalabra);

			caracteresElegidos = pedirCaracter(caracteresElegidos, input);

			fallos = 0;

			encontrado = false;

			fallos = numerarFallos(encontrado, caracteresElegidos, caracteresPalabra, fallos);

			generacionHombreMuerto(fallos);

			mensajePerder(fallos, FALLOS, palabraSecreta);

			acertado = true;

			acertado = determinarAcierto(caracteresPalabra, acertado);

			mensajeAcierto(acertado);

		} while (!acertado && fallos < FALLOS);

	}

	public static void inicializarArrays(char[][] caracteresPalabra2, String palabraSecreta) {

		caracteresPalabra[0] = palabraSecreta.toCharArray();
		caracteresPalabra[1] = new char[caracteresPalabra[0].length];

	}

	public static void mensajeAcierto(boolean acertado) {

		if (acertado)
			System.out.println("Has Acertado ");

	}

	public static boolean determinarAcierto(char[][] caracteresPalabra, boolean acertado) {

		for (int i = 0; i < caracteresPalabra[0].length; i++) {

			if (caracteresPalabra[1][i] != COINCIDENCIA) {
				acertado = false;
				break;
			}
		}

		return acertado;

	}

	public static void mensajePerder(int fallos, byte fallos3, String palabraSecreta) {

		if (fallos >= FALLOS) {
			System.out.println("Has perdido: " + palabraSecreta);
		}

	}

	public static void generacionHombreMuerto(int fallos) {

		switch (fallos) {
		case 1:

			System.out.println("     ___");
			break;
		case 2:

			System.out.println("      |");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 3:
			System.out.println("  ____ ");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 4:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 5:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 6:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println(" T    |");
			System.out.println("     ___");
			break;
		case 7:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println(" T    |");
			System.out.println(" A   ___");
			break;
		}

	}

	public static int numerarFallos(boolean encontrado, String caracteresElegidos, char[][] caracteresPalabra,
			int fallos) {

		for (int j = 0; j < caracteresElegidos.length(); j++) {

			encontrado = false;

			for (int i = 0; i < caracteresPalabra[0].length; i++) {
				if (caracteresPalabra[0][i] == caracteresElegidos.charAt(j)) {
					caracteresPalabra[1][i] = COINCIDENCIA;
					encontrado = true;
				}
			}

			if (!encontrado)

				fallos++;
		}

		return fallos;

	}

	public static String pedirCaracter(String caracteresElegidos, Scanner input) {

		System.out.println("Introduce una letra o acierta la palabra");
		System.out.println("Caracteres Elegidos: " + caracteresElegidos);
		caracteresElegidos += input.nextLine().toUpperCase();

		return caracteresElegidos;

	}

	public static void generarInterfaz(char[][] caracteresPalabra) {

		System.out.println("####################################");

		for (int i = 0; i < caracteresPalabra[0].length; i++) {
			if (caracteresPalabra[1][i] != COINCIDENCIA) {
				System.out.print(RAYA);
			} else {
				System.out.print(ESPACIO + caracteresPalabra[0][i]);
			}
		}
		System.out.println();

	}

	public static String obtencionPalabra(String palabraSecreta) {

		palabraSecreta = arrayPalabras[(int) (Math.random() * NUM_PALABRAS)];

		return palabraSecreta;

	}

	public static void llenarArrayPalabras(String[] palabras, String RUTA) {

		File fich = new File(RUTA);
		Scanner inputFichero = null;

		try {
			inputFichero = new Scanner(fich);
			for (int i = 0; i < NUM_PALABRAS; i++) {
				palabras[i] = inputFichero.nextLine();
			}
		} catch (Exception e) {
			System.out.println("Error al abrir fichero: " + e.getMessage());
		} finally {
			if (fich != null && inputFichero != null)
				inputFichero.close();
		}

	}

}