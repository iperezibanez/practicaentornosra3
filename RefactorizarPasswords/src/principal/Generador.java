//LLEVARNOS LOS M�TODOS A OTRA CLASE. 

package principal;

import java.util.Scanner;

public class Generador {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		menu();

		int longitud = longitud(scanner);

		int opcion = tipoPassword(scanner);

		String password = "";

		eleccion(opcion, longitud, password);

		scanner.close();
	}

	public static void eleccion(int opcion, int longitud, String password) {

		switch (opcion) {

		case 1:

			System.out.println(soloCaracteres(longitud, password));

			break;

		case 2:

			System.out.println(soloNumeros(longitud, password));

			break;

		case 3:

			System.out.println(letrasCaracteresEspeciales(longitud, password));

			break;

		case 4:

			System.out.println(letrasNumerosEspeciales(longitud, password));

			break;

		}

	}

	public static int tipoPassword(Scanner scanner) {

		System.out.println("Elige tipo de password: ");
		int opcion = scanner.nextInt();

		return opcion;

	}

	public static int longitud(Scanner scanner) {

		System.out.println("Introduce la longitud de la cadena: ");
		int longitud = scanner.nextInt();

		return longitud;

	}

	public static void menu() {

		System.out.println("Programa que genera passwords de la longitud indicada, y del rango de caracteres");
		System.out.println("1 - Caracteres desde A - Z");
		System.out.println("2 - Numeros del 0 al 9");
		System.out.println("3 - Letras y caracteres especiales");
		System.out.println("4 - Letras, numeros y caracteres especiales");

	}

	public static String letrasNumerosEspeciales(int longitud, String password) {

		for (int i = 0; i < longitud; i++) {

			if (numeroAleatorio2() == 1) {

				password += elegirLetraReal();

			} else if (numeroAleatorio2() == 2) {

				password += elegirCaracterEspecialReal();

			} else {

				password += elegirNumeroReal();
			}
		}

		return password;

	}

	public static int numeroAleatorio2() {

		int n;
		n = (int) (Math.random() * 3);

		return n;

	}

	public static String letrasCaracteresEspeciales(int longitud, String password) {

		for (int i = 0; i < longitud; i++) {

			if (numeroAleatorio() == 1) {

				password += elegirLetraReal();

			} else {

				password += elegirCaracterEspecialReal();

			}
		}

		return password;

	}

	public static char elegirCaracterEspecialReal() {

		char caracter3;
		caracter3 = (char) ((Math.random() * 15) + 33);

		return caracter3;

	}

	public static int numeroAleatorio() {

		int n;
		n = (int) (Math.random() * 2);
		return n;

	}

	public static String soloNumeros(int longitud, String password) {

		for (int i = 0; i < longitud; i++) {

			password += elegirNumeroReal();

		}

		return (password);

	}

	public static int elegirNumeroReal() {

		int numero;
		numero = (int) (Math.random() * 10);

		return numero;

	}

	public static String soloCaracteres(int longitud, String password) {

		for (int i = 0; i < longitud; i++) {

			password += elegirLetraReal();

		}

		return password;

	}

	public static char elegirLetraReal() {

		char letra;
		letra = (char) ((Math.random() * 26) + 65);

		return letra;

	}

}
